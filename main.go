package main

import (
	"routes"

	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func setupRouter() *gin.Engine {
	//gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	health := router.Group("/health")
	{
		health.GET("/liveness", routes.Liveness)
		health.GET("/readiness", routes.Readiness)
	}

	v1 := router.Group("/api/v1/files")
	{
		v1.POST("/post", routes.CreateFile)
		v1.POST("/get/:sha256", routes.Getfile)

	}
	return router
}

// main function as an entry point
func main() {
	router := setupRouter()
	router.Run()
}
