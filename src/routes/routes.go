package routes

import (
	"models"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	db  *gorm.DB
	err error
)

func init() {
	//open a db connection
	var err error
	db, err = gorm.Open("mysql", "root:123456@(127.0.0.1)/demo?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}
	//Migrate the schema
	db.AutoMigrate(&models.FileModel{})
}

// Liveness function that indicates service liveness
func Liveness(c *gin.Context) {
	c.JSON(200, gin.H{"Status": "Live"})
}

// Readiness handler that test connection and query from the DB
func Readiness(c *gin.Context) {
	var err error
	var file models.FileModel
	// if err = db.First(&todo, 1).Error; err != nil {
	if err = db.Limit(1).Find(&file).Error; err != nil {
		c.JSON(500, gin.H{"Status": "Not Ready"})
		return
	}
	c.JSON(200, gin.H{"Status": "Ready"})
}

func Getfile(c *gin.Context) {
	var file models.FileModel
	sha256 := c.Param("sha256")
	if len(sha256) != 64 {
		c.JSON(http.StatusBadRequest, gin.H{"msg": "Bad Format", "hash": sha256})
		return
	}
	if err := db.Where("sha256 = ?", sha256).First(&file).Error; err != nil {
		c.JSON(400, gin.H{"msg": "Hash Not Found", "hash": sha256})
		return
	}
	c.JSON(200, gin.H{"Status": file.Sha256, "Detected": file.Detected})
}

func CreateFile(c *gin.Context) {
	var file models.FileModel
	c.BindJSON(&file)
	file = models.FileModel{Sha256: file.Sha256, Detected: file.Detected, Submissions: file.Submissions}
	if err := db.Save(&file).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusNotFound, "message": "Failed to insert record"})
		return
	}
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Todo item created successfully!", "resourceId": file.Sha256})
}
