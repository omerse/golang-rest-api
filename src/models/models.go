package models

import "github.com/jinzhu/gorm"

type (
	// FileModel struct
	FileModel struct {
		gorm.Model
		Sha256      string `json:"sha256" binding:"required" gorm:"primary_key size:64"`
		Detected    bool   `json:"detected"`
		FileSha256  string `uri:"sha256"`
		Submissions int16  `json:"submissions"`
	}
)
