package main

import (
	"log"

	"github.com/imroc/req"
)

var err error

type File struct {
	sha256      string
	detected    bool
	submissions int16
}

func main() {
	var file = File{sha256: "b020052d4b69a8b1c1f7450d56fe088e892f5b305bae6617daf7ed82dec0be2a", detected: true, submissions: 203}

	// header := req.Header{
	// "Accept":        "application/json",
	// "Authorization": "Basic YWRtaW46YWRtaW4=",
	//	}
	// only url is required, others are optional.
	r, err := req.Post("http://localhost:8080/api/v1/files/post", req.BodyJSON(&file))
	if err != nil {
		log.Fatal(err)
	}
	//r.ToJSON(&foo)       // response => struct/map
	log.Printf("%+v", r) // print info (try it, you may surprise)
}
